---
title: "About"
layout: "about"
# url: "/about"
summary: "about"
---

Softwares created
-----------------

- [comin](https://github.com/nlewo/comin) - Git Push NixOS Machines
- [nix2container](https://github.com/nlewo/nix2container) - Build OCI container images from Nix expressions
- [Markas](https://framagit.org/markas/infrastructure/) - The infrastructure of a NPO providing cloud services
- [nixpkgs-swh](https://github.com/nix-community/nixpkgs-swh) - Send nixpkgs tarballs to Software Heritagean Hydra API client
- [swayctl](https://github.com/nlewo/swayctl) - The [Xmonad Dynamic Workspace](https://hackage.haskell.org/package/xmonad-contrib-0.16/docs/XMonad-Actions-DynamicWorkspaces.html) behavior for Sway
- [hydra-cli](https://github.com/nlewo/hydra-cli) - An Hydra API client
- [Gremnix](https://github.com/nlewo/gremnix) - Browse the references graph of Nix store paths with the Gremlin language

Softwares maintaining
---------------------

- [NixOS Mailserver](https://gitlab.com/simple-nixos-mailserver/nixos-mailserver) - A complete and Simple Nixos Mailserver
- [nixpkgs](https://github.com/nixos/nixpkgs) - Committer since 2018
- [Gandi Terraform Provider](https://github.com/go-gandi/terraform-provider-gandi) - The unofficial Gandi Terraform Provider

Public Talks
------------

- Nixcon 2022 - [nix2container: faster container image rebuilds](https://www.youtube.com/watch?app=desktop&v=ELeMGbFjtTo)
