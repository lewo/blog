---
title: How to install NixOS on kimsufi
date: 2019-12-01T00:00:00+00:00
---

Here are some notes on how to install NixOS 19.09 on a Kimsufi
dedicated server.

## From the kimsufi webmail interface

- Go to the Kimsufi webui
- Choose netboot and select the resuce64pro
- Reboot the server
- Use the root credentials sent by mail from OVH
- SSH to the server with above credentials

## From the server rescue mode

The following links can be useful:

- [https://nixos.org/nixos/manual/index.html#sec-installing-from-other-distro](https://nixos.org/nixos/manual/index.html#sec-installing-from-other-distro)
- [https://greizgh.github.io/blog/nixos-kimsufi/](https://greizgh.github.io/blog/nixos-kimsufi/)

In this procedure, we use the BIOS mode.

     # Create the FS layout
     parted /dev/sda -- mklabel msdos
     parted /dev/sda -- mkpart primary 1MiB 200GiB
     parted /dev/sda -- mkpart primary 200GiB -8GiB
     parted /dev/sda -- mkpart primary linux-swap -8GiB 100%
     mkfs.ext4 -L nixos /dev/sda1
     mkswap -L swap /dev/sda2
     mount /dev/disk/by-label/nixos /mnt
     swapon /dev/sda3

     # Create users
     groupadd -g 30000 nixbld
     useradd -u 30000 -g nixbld -G nixbld nixbld

     useradd -m setupuser
     passwd setupuser
     sed s/root/setupuser/ /etc/sudoers -i
     su setupuser -s /bin/bash

     # Install Nix
     curl https://nixos.org/nix/install | sh
     . $HOME/.nix-profile/etc/profile.d/nix.sh

     nix-channel --add https://nixos.org/channels/nixos-19.09 nixpkgs
     nix-channel --update

     # Install NixOS
     sudo `which nixos-generate-config` --root /mnt
     # Now we need to copy a configuration.nix file to /mnt/etc/nixos/configuration.nix

     export PATH=/usr/sbin:/sbin:$PATH
     sudo PATH="$PATH" NIX_PATH="$NIX_PATH" `which nixos-install` --root /mnt

Once installed, we need to choose to boot from the hard drive in the Kimsufi webmail, and reboot the server.
