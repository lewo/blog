To generate it

`nix-shell --run "ghc --make site.hs; ./site build"`

Generated pages are in `_site` directory.